﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PkRmsSapIntegrationAPI.Entities
{
    [Table("Location")]
    public class Location
    {
        [Key]
        public int Location_OID
        {
            get;
            set;
        }
        
        public string Active_f
        {
            get;
            set;
        }
        
        public string Location_Name
        {
            get;
            set;
        }

        

    }
}
