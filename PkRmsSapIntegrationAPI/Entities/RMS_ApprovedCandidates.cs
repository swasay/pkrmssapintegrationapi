﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PkRmsSapIntegrationAPI.Entities
{
    [Table("OA.RMS_ApprovedCandidates")]
    public class RMS_ApprovedCandidates
    {
        [Key]
        public int ID { get; set; }
        public int Unique_OID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CNIC { get; set; }
        public DateTime? Birth_Date { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Employer { get; set; }
        public string Location { get; set; }
        public string JobTitle { get; set; }
        public string JobTitleId { get; set; }
        public string Sex { get; set; }
        public string Create_By { get; set; }
        public DateTime Create_Date { get; set; }
        public string Modify_By { get; set; }
        public DateTime? Modify_Date { get; set; }
        public DateTime? ShortlistedDate { get; set; }
        public int OnboardingStatusCode { get; set; }
        public string Source { get; set; }
    }
}
