﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PkRmsSapIntegrationAPI.Entities
{
    [Table("OA.RMS_Locations")]
    public class RMS_Locations
    {
        [Key]
        public string RMS_LocationCode { get; set; }
        public string BPOLocationCode { get; set; }
        public string Name { get; set; }
    }
}
