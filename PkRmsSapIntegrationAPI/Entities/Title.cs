﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PkRmsSapIntegrationAPI.Entities
{
    [Table("Core.Title")]
    public class Title
    {
        [Key]
        public int Title_OID
        {
            get;
            set;
        }
        public string TitleName
        {
            get;
            set;
        }

        public int Active_f
        {
            get;
            set;
        }

    }
}
