﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PkRmsSapIntegrationAPI.Entities
{
    [Table("OA.RMS_Titles")]
    public class RMS_Titles
    {
        [Key]
        public string RMS_TitleCode { get; set; }
        public int BPOTitle_OID { get; set; }
        public string Name { get; set; }
        public string RMS_Title { get; set; }
        public string BPOLegalEntityCode { get; set; }

    }
}
