﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PkRmsSapIntegrationAPI.Entities
{
    [Table("OA.RMS_LegalEntities")]
    public class RMS_LegalEntities
    {
        [Key]
        public string RMS_LegalEntityCode { get; set; }
        public int BPOLegalEntityCode { get; set; }
        public string Name { get; set; }
    }
}
