﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggingFacilitator.Logging;

namespace PkRmsSapIntegrationAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LogManager.Info("Initialize..");
            ApiClient.initialize();
        }
    }
}
