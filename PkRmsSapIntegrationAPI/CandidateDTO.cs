﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PkRmsSapIntegrationAPI
{
    public class CandidateDTO
    {
        public int lead_id { get; set; }
        public string candidate_name { get; set; }
        public string contact_number { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
        public string cnic { get; set; }
        public DateTime dob { get; set; }
        public DateTime short_listed_date { get; set; }
        public string rms_job_title_id { get; set; }
        public string rms_job_title { get; set; }
        public string hris_job_title { get; set; }
        public string entity_name { get; set; }
        public string entity_code { get; set; }
        public string location_id { get; set; }
        public string location { get; set; }
        public string final_interview_status { get; set; }
        public int? OnboardingStatusCode { get; set; }
        public string comments { get; set; }
    }
}
