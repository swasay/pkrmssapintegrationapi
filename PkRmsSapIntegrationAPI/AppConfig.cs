﻿using System.Collections.Generic;
using System.Configuration;

namespace PkRmsSapIntegrationAPI
{
    public class AppConfig
    {
        private static int? _intervalMinutes;
        public static int IntervalMinutes
        {
            get
            {
                if (!_intervalMinutes.HasValue)
                    _intervalMinutes = int.Parse(ConfigurationManager.AppSettings["IntervalMinutes"].ToString());
                return _intervalMinutes.Value;
            }
        }
        private static string _pkRMSSourceId;
        public static string PkRMSSourceId
        {
            get
            {
                if (string.IsNullOrEmpty(_pkRMSSourceId))
                    _pkRMSSourceId = ConfigurationManager.AppSettings["PkRMSSourceId"].ToString();
                return _pkRMSSourceId;
            }
        }
        private static int? _invalidStatusCode;
        public static int InvalidStatusCode
        {
            get
            {
                if (!_invalidStatusCode.HasValue)
                    _invalidStatusCode = int.Parse(ConfigurationManager.AppSettings["InvalidStatusCode"].ToString());
                return _invalidStatusCode.Value;
            }
        }

        private static string _webAPIUrl;
        public static string WebAPIUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_webAPIUrl))
                    _webAPIUrl = ConfigurationManager.AppSettings["WebAPIUrl"];
                return _webAPIUrl;
            }
        }

        private static string _apiAuthUser;
        public static string APIAuthUser
        {
            get
            {
                if (string.IsNullOrEmpty(_apiAuthUser))
                    _apiAuthUser = ConfigurationManager.AppSettings["APIAuthUser"];
                return _apiAuthUser;
            }
        }

        private static string _apiAuthPwd;
        public static string APIAuthPwd
        {
            get
            {
                if (string.IsNullOrEmpty(_apiAuthPwd))
                    _apiAuthPwd = ConfigurationManager.AppSettings["APIAuthPwd"];
                return _apiAuthPwd;
            }
        }

        private static string _apiBasicAuthKey;
        public static string APIBasicAuthKey
        {
            get
            {
                if (string.IsNullOrEmpty(_apiBasicAuthKey))
                    _apiBasicAuthKey = ConfigurationManager.AppSettings["APIBasicAuthKey"];
                return _apiBasicAuthKey;
            }
        }

        private static string _updateExistingCandidates;
        public static string UpdateExistingCandidates
        {
            get
            {
                if (string.IsNullOrEmpty(_updateExistingCandidates))
                    _updateExistingCandidates = ConfigurationManager.AppSettings["UpdateExistingCandidates"];
                return _updateExistingCandidates;
            }
        }

        private static string _wflwstate;
        public static string Wflwstate
        {
            get
            {
                if (string.IsNullOrEmpty(_wflwstate))
                    _wflwstate = ConfigurationManager.AppSettings["Wflwstate"].ToString();
                return _wflwstate;
            }
        }

        private static int? _pastDays;
        public static int PastDays
        {
            get
            {
                if (!_pastDays.HasValue)
                    _pastDays = int.Parse(ConfigurationManager.AppSettings["PastDays"].ToString());
                return _pastDays.Value;
            }
        }

        private static bool? _isUsingProxy;
        public static bool IsUsingProxy
        {
            get
            {
                if (!_isUsingProxy.HasValue)
                    _isUsingProxy = ConfigurationManager.AppSettings["IsUsingProxy"] == "1";
                return _isUsingProxy.Value;
            }
        }

        private static string _proxyUrl;
        public static string ProxyUrl
        {
            get
            {
                if (string.IsNullOrEmpty(_proxyUrl))
                    _proxyUrl = ConfigurationManager.AppSettings["ProxyUrl"];
                return _proxyUrl;
            }
        }

        private static string _proxyPort;
        public static string ProxyPort
        {
            get
            {
                if (string.IsNullOrEmpty(_proxyPort))
                    _proxyPort = ConfigurationManager.AppSettings["ProxyPort"];
                return _proxyPort;
            }
        }

        private static string _proxyUserName;

        public static string ProxyUserName
        {
            get
            {
                if (string.IsNullOrEmpty(_proxyUserName))
                    _proxyUserName = ConfigurationManager.AppSettings["ProxyUsername"];
                return _proxyUserName;
            }
        }

        private static string _proxyPassword;

        public static string ProxyPassword
        {
            get
            {
                if (string.IsNullOrEmpty(_proxyPassword))
                    _proxyPassword = ConfigurationManager.AppSettings["ProxyPassword"];
                return _proxyPassword;
            }
        }

        private static int? _fetchedStatusCode;

        public static int FetchedStatusCode
        {
            get
            {
                if (!_fetchedStatusCode.HasValue)
                    _fetchedStatusCode = int.Parse(ConfigurationManager.AppSettings["FetchedStatusCode"].ToString());
                return _fetchedStatusCode.Value;
            }
        }

        private static string _debug_f;

        public static string Debug_F
        {
            get
            {
                if (string.IsNullOrEmpty(_debug_f))
                    _debug_f = ConfigurationManager.AppSettings["Debug_F"];
                return _debug_f;
            }
        }


        private static int? _timeout_Intervals;
        public static int TimeOut_Intervals
        {
            get
            {
                if (!_timeout_Intervals.HasValue)
                    _timeout_Intervals = int.Parse(ConfigurationManager.AppSettings["Time_Interval"].ToString());
                return _timeout_Intervals.Value;
            }
        }

        private static List<string> _validStatuses;
        public static List<string> ValidWorkflowStatuses
        {
            get
            {
                char[] sep = new char[1];
                sep[0] = ',';
                if (_validStatuses == null || _validStatuses.Count == 0)
                {
                    _validStatuses = new List<string>(Wflwstate.Replace(" ", string.Empty).Split(sep, System.StringSplitOptions.RemoveEmptyEntries));
                }

                return _validStatuses;
            }
        }

        private static int? _jobStatusCheckPeriod;
        public static int JobStatusCheckPeriod
        {
            get
            {
                int valHolder = 0;
                if (!_jobStatusCheckPeriod.HasValue)
                {
                    int.TryParse(ConfigurationManager.AppSettings["JobStatusCheckPeriod"].ToString(), out valHolder);
                    _jobStatusCheckPeriod = valHolder;
                }

                return _jobStatusCheckPeriod.Value;
            }
        }
    }
}
