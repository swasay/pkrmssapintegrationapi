﻿using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using PkRmsSapIntegrationAPI.Entities;

namespace PkRmsSapIntegrationAPI
{
    public partial class RmsIntegrationContext : DbContext
    {
        public RmsIntegrationContext()
            : base("name=RmsIntegrationContext")
        {
        }

        public virtual DbSet<RMS_Locations> RMS_Locations { get; set; }
        public virtual DbSet<Location> Location { get; set; }
        public virtual DbSet<RMS_Titles> RMS_Titles { get; set; }
        public virtual DbSet<Title> Title { get; set; }
        public virtual DbSet<RMS_LegalEntities> RMS_LegalEntities { get; set; }
        public virtual DbSet<RMS_ApprovedCandidates> RMS_ApprovedCandidates { get; set; }
        //public virtual DbSet<SilkRoad_LegalEntities> SilkRoad_LegalEntities { get; set; }
        //public virtual DbSet<SilkRoadLocation> SilkRoad_Locations { get; set; }
        //public virtual DbSet<SilkRoad_DepartmentMapping> SilkRoad_DepartmentMapping { get; set; }
        //public virtual DbSet<SilkRoad_EmployeeStatusMapping> SilkRoad_EmployeeStatusMapping { get; set; }
        //public virtual DbSet<SilkRoad_LegalEntityMapping> SilkRoad_LegalEntityMapping { get; set; }
        //public virtual DbSet<SilkRoadLocationMapping> SilkRoad_LocationMapping { get; set; }
        //public virtual DbSet<BpoEmployee> BpoEmployees { get; set; }

        //public virtual DbSet<Log> Logs { get; set; }



    }
}
