﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using PkRmsSapIntegrationAPI.Entities;
using System.Data.SqlClient;
using LoggingFacilitator.Logging;

namespace PkRmsSapIntegrationAPI
{
    public class ApiClient
    {
        private static WebRequest request;
        public static int timeInterval = AppConfig.TimeOut_Intervals;
        private static void AddAuthentication()
        {
            LogManager.Info("Adding Authentication");
            request.Method = "POST";
            request.Headers["Authorization"] = "Basic " + AppConfig.APIBasicAuthKey;
            request.Headers.Add("username:" + AppConfig.APIAuthUser);
            request.Headers.Add("password:" + AppConfig.APIAuthPwd);
        }

        private static WebRequest GetAuthenticatedReq(string url)
        {
            WebRequest returnable = WebRequest.Create(url);
            returnable.Method = "GET";
            returnable.Headers["Authorization"] = "Basic " + AppConfig.APIBasicAuthKey;
            returnable.Credentials = new NetworkCredential(AppConfig.APIAuthUser, AppConfig.APIAuthPwd);
            returnable.Timeout = timeInterval;

            if (AppConfig.IsUsingProxy)
            {
                int port = 0;
                int.TryParse(AppConfig.ProxyPort, out port);
                WebProxy proxy = new WebProxy(AppConfig.ProxyUrl, port);
                proxy.Credentials = new NetworkCredential(AppConfig.ProxyUserName, AppConfig.ProxyPassword);
                proxy.UseDefaultCredentials = true;
                returnable.Proxy = proxy;
            }

            return returnable;
        }
        public static void initialize()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 |
                                                                  System.Net.SecurityProtocolType.Tls11 |
                                                                  System.Net.SecurityProtocolType.Tls;
            ApiClient.GetData();
        }
        public static void GetData()
        {
            string url = AppConfig.WebAPIUrl + "?start_date=" + DateTime.Now.AddDays(AppConfig.PastDays).ToString("yyyy-MM-dd") + "&end_date=" + DateTime.Today.ToString("yyyy-MM-dd");
            LogManager.Info("Creating web request");
            request = WebRequest.Create(url);

            request.Timeout = timeInterval;

            AddAuthentication();

            try
            {
                LogManager.Info("Waiting for response..");
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    LogManager.Info("Getting response stream");
                    using (var sr = new System.IO.StreamReader(response.GetResponseStream()))
                    {
                        try
                        {
                            var data = sr.ReadToEnd();
                            JObject joResponse = JObject.Parse(data);
                            if (joResponse.Property("message").ToString().ToLower().Equals("no data found."))
                            {
                                LogManager.Info(joResponse.Property("message").Value.ToString());
                            }
                            else
                            {
                                JArray array = (JArray)joResponse["data"];
                                var converted = array.ToObject<List<CandidateDTO>>();
                                SaveLocations(converted);
                                SaveJobs(converted);
                                SaveCandidates(converted);
                                ValidateSavedData();
                                MoveRMSDataToSAP();
                            }
                            LogManager.Info("Process complete");
                        }
                        catch (Exception ex)
                        {
                            LogManager.Error("Error in GetData() function while reading response stream: " + ex.StackTrace);
                            LogManager.Error(ex.InnerException == null ? string.Empty : ex.InnerException.ToString() + ">>>>" + ex.InnerException.StackTrace + ">>>>");
                        }
                    }
                }


            }
            catch (WebException ex)
            {
                LogManager.Error("Error in GetData() function while reading response stream: " + ex.StackTrace);
                LogManager.Error(ex.InnerException == null ? string.Empty : ex.InnerException.ToString() + ">>>>" + ex.InnerException.StackTrace + ">>>>");
            }
        }
        public static void ValidateSavedData()
        {
            try
            {
                using (var localContext = new RmsIntegrationContext())
                {
                    LogManager.Info("Going to Validate Records");
                    localContext.Database.ExecuteSqlCommand("EXEC [OA].[MarkInvalidRMSRecords]");
                    LogManager.Info("Validated Records");
                }
            }
            catch (Exception ex)
            {
                LogManager.Error("Error in ValidateSavedData() function: " + ex.Message);
                LogManager.Error(ex.StackTrace);
                LogManager.Error(ex.InnerException == null ? string.Empty : ex.InnerException.Message + ": " + ex.InnerException.ToString() + ">>>>" + ex.InnerException.StackTrace + ">>>>");
            }
        }
        public static void MoveRMSDataToSAP()
        {
            try
            {
                using (var localContext = new RmsIntegrationContext())
                {
                    LogManager.Info("Migrating data to RMS");
                    localContext.Database.ExecuteSqlCommand("EXEC [OA].[usp_MigrateRMSPKToRMS]");
                    LogManager.Info("Succesfully Migrated data to RMS");
                }
            }
            catch (Exception ex)
            {
                LogManager.Error("Error in MoveRMSDataToSAP() function: " + ex.Message);
                LogManager.Error(ex.StackTrace);
                LogManager.Error(ex.InnerException == null ? string.Empty : ex.InnerException.Message + ": " + ex.InnerException.ToString() + ">>>>" + ex.InnerException.StackTrace + ">>>>");
            }
        }
        public static void SaveCandidates(List<CandidateDTO> data)
        {
            try
            {
                using (var localContext = new RmsIntegrationContext())
                {
                    LogManager.Info("Going to Save Candidates data");
                    foreach (var item in data)
                    {
                        var exist = localContext.RMS_ApprovedCandidates.Where(x => x.Unique_OID == item.lead_id && x.Email.Equals(item.email));
                        var name = item.candidate_name;
                        if (exist.Count() == 0)
                        {
                            RMS_ApprovedCandidates candidate = new RMS_ApprovedCandidates();
                            candidate.Unique_OID = item.lead_id;
                            candidate.ShortlistedDate = item.short_listed_date;
                            candidate.Sex = item.gender.ToLower() == "male" ? "M" : "F";
                            candidate.Phone = item.contact_number;
                            candidate.Location = item.location_id;
                            candidate.FirstName = name.Substring(0, name.IndexOf(" "));
                            candidate.LastName = name.Substring(name.LastIndexOf(" ") + 1);
                            candidate.JobTitle = item.rms_job_title_id;
                            candidate.Employer = item.entity_code;
                            candidate.Email = item.email;
                            candidate.CNIC = item.cnic;
                            candidate.Birth_Date = item.dob;
                            candidate.Create_By = "RMSJob";
                            candidate.Create_Date = DateTime.Now;
                            candidate.OnboardingStatusCode = AppConfig.FetchedStatusCode;
                            candidate.Source = AppConfig.PkRMSSourceId;
                            localContext.RMS_ApprovedCandidates.Add(candidate);
                        }else
                        {
                            var candidate = exist.FirstOrDefault();
                            if (candidate.OnboardingStatusCode == AppConfig.InvalidStatusCode)
                            {
                                candidate.ShortlistedDate = item.short_listed_date;
                                candidate.Sex = item.gender.ToLower() == "male" ? "M" : "F";
                                candidate.Phone = item.contact_number;
                                candidate.Location = item.location_id;
                                candidate.FirstName = name.Substring(0, name.IndexOf(" "));
                                candidate.LastName = name.Substring(name.LastIndexOf(" ") + 1);
                                candidate.JobTitle = item.rms_job_title_id;
                                candidate.Employer = item.entity_code;
                                candidate.CNIC = item.cnic;
                                candidate.Birth_Date = item.dob;
                                candidate.Modify_By = "RMSJob";
                                candidate.Modify_Date = DateTime.Now;
                                candidate.OnboardingStatusCode = AppConfig.FetchedStatusCode;
                                candidate.Source = AppConfig.PkRMSSourceId;
                            }
                        }
                    }
                    localContext.SaveChanges();
                    LogManager.Info("Saved candidates data ");
                }
            }
            catch (Exception ex)
            {
                LogManager.Error("Error in SaveCandidates() function while saving Candidates: " + ex.Message);
                LogManager.Error(ex.StackTrace);
                LogManager.Error(ex.InnerException == null ? string.Empty : ex.InnerException.Message + ": " + ex.InnerException.ToString() + ">>>>" + ex.InnerException.StackTrace + ">>>>");
            }
        }
        public static void SaveJobs(List<CandidateDTO> data)
        {
            try
            {
                using (var localContext = new RmsIntegrationContext())
                {
                    LogManager.Info("Going to Save Job Titles");
                    var updatedt = from nl in data
                                   join l in localContext.RMS_Titles
                                   on nl.rms_job_title equals l.RMS_TitleCode
                                   where l.BPOTitle_OID == 0
                                   select l;

                    foreach (var tt in updatedt)
                    {
                        var idloc = localContext.Title.Where(l => tt.Name.ToLower().Trim().Equals(l.TitleName.ToLower().Trim())).FirstOrDefault();
                        tt.BPOTitle_OID = idloc == null ? 0 : idloc.Title_OID;
                    }
                    var noexistt = data.Where(x => !localContext.RMS_Titles.Any(y => y.RMS_TitleCode.Equals(x.rms_job_title_id)))
                                .Select(x => new RMS_Titles
                                {
                                    RMS_TitleCode = x.rms_job_title_id,
                                    Name = x.hris_job_title,
                                    BPOLegalEntityCode = x.entity_code,
                                    RMS_Title = x.rms_job_title
                                })
                                .GroupBy(l => l.RMS_TitleCode)
                                .Select(group => group.First());
                   
                    var final = from nl in noexistt
                                join l in localContext.Title
                                on new { Name = nl.Name.ToLower().Trim(), Active_f = 1 } equals new { Name = l.TitleName.ToLower().Trim(), Active_f = l.Active_f } into tt
                                from jt in tt.DefaultIfEmpty()
                                select new RMS_Titles
                                {
                                    BPOTitle_OID = jt==null?0:jt.Title_OID,
                                    RMS_TitleCode = nl.RMS_TitleCode,
                                    Name = nl.Name,
                                    BPOLegalEntityCode = nl.BPOLegalEntityCode,
                                    RMS_Title = nl.RMS_Title
                                };

                    foreach (var t in final)
                    {
                        localContext.RMS_Titles.Add(t);
                    }

                    localContext.SaveChanges();
                    LogManager.Info("Saved job titles");
                }
            }
            catch (Exception ex)
            {
                LogManager.Error("Error in SaveJobs() function while saving Candidates: " + ex.Message);
                LogManager.Error(ex.StackTrace);
                LogManager.Error(ex.InnerException == null ? string.Empty : ex.InnerException.Message + ": " + ex.InnerException.ToString() + ">>>>" + ex.InnerException.StackTrace + ">>>>");
            }
        }
        public static void SaveLocations(List<CandidateDTO> data)
        {
            try
            {
                using (var localContext = new RmsIntegrationContext())
                {
                    LogManager.Info("Going to Save Locations");
                    var updatedloc = from nl in data
                                     join l in localContext.RMS_Locations
                                     on nl.location_id equals l.RMS_LocationCode
                                     where l.BPOLocationCode == "0"
                                     select l;

                    foreach (var loc in updatedloc)
                    {
                        var idloc = localContext.Location.Where(l => loc.Name.ToLower().Trim().Equals(l.Location_Name.ToLower().Trim())).FirstOrDefault();
                        loc.BPOLocationCode = idloc == null ? "0" : idloc.Location_OID.ToString();
                    }

                    var noexistloc = data.Where(x => !localContext.RMS_Locations.Any(y => y.RMS_LocationCode.Equals(x.location_id)))
                                .Select(x => new RMS_Locations
                                {
                                    RMS_LocationCode = x.location_id,
                                    Name = x.location
                                })
                                .GroupBy(l => l.RMS_LocationCode)
                                .Select(group => group.First());


                    var final = from nl in noexistloc
                                join l in localContext.Location
                                on new { Name = nl.Name.ToLower().Trim(), Active_f = "A" } equals new { Name = l.Location_Name.ToLower().Trim(), Active_f = l.Active_f } into tt
                                from loc in tt.DefaultIfEmpty()
                                select new RMS_Locations
                                {
                                    BPOLocationCode = loc==null?"0":loc.Location_OID.ToString(),
                                    RMS_LocationCode = nl.RMS_LocationCode,
                                    Name = nl.Name
                                };
                    if (final != null)
                    {
                        foreach (var loc in final)
                        {
                            localContext.RMS_Locations.Add(loc);
                        }
                    }
                    localContext.SaveChanges();
                    LogManager.Info("Saved locations");
                }
            }
            catch (Exception ex)
            {
                LogManager.Error("Error in SaveLocations() function while saving Candidates: " + ex.Message);
                LogManager.Error(ex.StackTrace);
                LogManager.Error(ex.InnerException == null ? string.Empty : ex.InnerException.Message + ": " + ex.InnerException.ToString() + ">>>>" + ex.InnerException.StackTrace + ">>>>");
            }
        }
    }
}
