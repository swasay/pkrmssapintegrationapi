﻿using LoggingFacilitator.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingFacilitator.Logging
{
    public static class LogManager
    {
        private static readonly ILoggerFacade _defaultLogger;

        static LogManager()
        {
            if (AppConfig.DefaultLoggerName.Equals("SeriLog", StringComparison.OrdinalIgnoreCase))
                _defaultLogger = new SeriLogLogger();
            else
                throw new Exception("No logger name configured.");
            //this should be injected by Unity/Ninject/etc later.

            _defaultLogger.SetUp();
        }

        public static ILoggerFacade GetLogger()
        {
            return _defaultLogger;
        }

        public static void Verbose(string msg, params object[] logObjs)
        {
            _defaultLogger.Verbose(msg, logObjs);
        }

        public static void Verbose<GenLoggerType>(string msg, params object[] logObjs) where GenLoggerType : ILoggerFacade,
                                                                                                             new()
        {
            ILoggerFacade givenLogger = new GenLoggerType();
            givenLogger.Verbose(msg, logObjs);
        }

        public static void Debug(string msg, params object[] logObjs)
        {
            _defaultLogger.Debug(msg, logObjs);
        }

        public static void Debug<GenLoggerType>(string msg, params object[] logObjs) where GenLoggerType : ILoggerFacade,
                                                                                                           new()
        {
            ILoggerFacade givenLogger = new GenLoggerType();
            givenLogger.Debug(msg, logObjs);
        }

        public static void Info<GenLoggerType>(string msg, params object[] logObjs) where GenLoggerType : ILoggerFacade,
                                                                                                           new()
        {
            ILoggerFacade givenLogger = new GenLoggerType();
            givenLogger.Info(msg, logObjs);
        }

        public static void Info(string msg, params object[] logObjs)
        {
            _defaultLogger.Info(msg, logObjs);
        }

        public static void Error<GenLoggerType>(string msg, Exception ex = null, params object[] logObjs) where GenLoggerType : ILoggerFacade,
                                                                                                                                 new()
        {
            ILoggerFacade givenLogger = new GenLoggerType();
            givenLogger.Error(msg, ex, logObjs);
        }

        public static void Error(string msg, Exception ex = null, params object[] logObjs)
        {
            _defaultLogger.Error(msg, ex, logObjs);
        }

        public static void Warning<GenLoggerType>(string msg, Exception ex = null, params object[] logObjs) where GenLoggerType : ILoggerFacade,
                                                                                                                                  new()
        {
            ILoggerFacade givenLogger = new GenLoggerType();
            givenLogger.Warning(msg, ex, logObjs);
        }

        public static void Warning(string msg, Exception ex = null, params object[] logObjs)
        {
            _defaultLogger.Warning(msg, ex, logObjs);
        }

        public static void Fatal<GenLoggerType>(string msg, Exception ex = null, params object[] logObjs) where GenLoggerType : ILoggerFacade,
                                                                                                                                new()
        {
            ILoggerFacade givenLogger = new GenLoggerType();
            givenLogger.Fatal(msg, ex, logObjs);
        }

        public static void Fatal(string msg, Exception ex = null, params object[] logObjs)
        {
            _defaultLogger.Fatal(msg, ex, logObjs);
        }

        public static void Dispose()
        {
            _defaultLogger.Dispose();
        }

        public static void Dispose<GenLoggerType>() where GenLoggerType : ILoggerFacade, new()
        {
            ILoggerFacade givenLogger = new GenLoggerType();
            givenLogger.Dispose();
        }

    }
}
