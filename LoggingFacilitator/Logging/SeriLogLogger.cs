﻿using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggingFacilitator.Logging
{
    public class SeriLogLogger : ILoggerFacade
    {
        public SeriLogLogger()
        {

        }

        public void SetUp()
        {
            Log.Logger = new LoggerConfiguration()
                            // .Enrich.
                            //.ReadFrom.AppSettings()
                            .WriteTo.File("./logs/log.txt", rollingInterval: RollingInterval.Day)
                            .WriteTo.Console()
                            .CreateLogger();
        }

        public void Verbose(string msg, params object[] logObjs)
        {
            Log.Verbose(msg, logObjs);
        }

        public void Debug(string msg, params object[] logObjs)
        {
            Log.Debug(msg, logObjs);
        }

        public void Info(string msg, params object[] logObjs)
        {
            Log.Information(msg, logObjs);
        }

        public void Error(string msg, Exception ex = null, params object[] logObjs)
        {
            Log.Error(ex, msg, logObjs);
        }

        public void Warning(string msg, Exception ex = null, params object[] logObjs)
        {
            Log.Warning(ex, msg, logObjs);
        }

        public void Fatal(string msg, Exception ex = null, params object[] logObjs)
        {
            Log.Fatal(ex, msg, logObjs);
        }

        public void Dispose()
        {
            Log.CloseAndFlush();
        }
    }
}
