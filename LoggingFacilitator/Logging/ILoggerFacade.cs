﻿using System;

namespace LoggingFacilitator.Logging
{
    public interface ILoggerFacade
    {
        void SetUp();

        void Verbose(string msg, params object[] logObjs);
        void Debug(string msg, params object[] logObjs);

        void Info(string msg, params object[] logObjs);

        void Error(string msg, Exception ex = null, params object[] logObjs);

        void Warning(string msg, Exception ex = null, params object[] logObjs);

        void Fatal(string msg, Exception ex = null, params object[] logObjs);

        void Dispose();
    }
}
